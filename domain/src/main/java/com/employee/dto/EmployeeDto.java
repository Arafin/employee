package com.employee.dto;

import com.employee.entity.Employee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmployeeDto {

	private Long id;

	private String employeeName;

	public static EmployeeDto select(Employee employee) {
		EmployeeDto dto = new EmployeeDto();
		dto.setId(employee.getId());
		dto.setEmployeeName(employee.getEmployeeName());
		return dto;
	}

}
