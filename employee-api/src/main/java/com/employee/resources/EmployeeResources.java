package com.employee.resources;

import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.dto.EmployeeDto;
import com.employee.service.EmployeeService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.employee.utils.ResponseBuilder.success;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "employee")
@Tag(name = "Employee API")
public class EmployeeResources {

	private final EmployeeService employeeService;

	@GetMapping("/lists")
	@Operation(summary = "show lists of employee", description = "lists of employee")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "found employee", content = {
			@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = EmployeeDto.class))) }) }
	)
	public ResponseEntity<JSONObject> findAll() {
		List<EmployeeDto> dtos = employeeService.findAll()
				.stream()
				.map(EmployeeDto::select)
				.collect(Collectors.toList());
		log.info("all employee lists {} ", dtos);
		return ok(success(dtos).getJson());
	}

}
